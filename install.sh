#!/bin/bash

clear
echo
echo
echo "[1] Ubuntu/Debian"
echo "[2] Arch"    
echo "[3] Sair"
echo -e -n "\nQual seu sistema ->> "
read os

if [ $os == "1" ]
  then 
	sudo apt-get update -y
	sudo apt-get install netcat -y
	apt-get install figlet -y
elif [ $os == "2" ]
  then
	pacman -S figlet
	pacman -S netcat
else
exit 0
fi
